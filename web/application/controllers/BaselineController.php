<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaselineController extends CI_Controller {
    
    var $GET;
    var $POST;
    var $DATA;
    var $account;

    public function __construct() {
        parent::__construct();
        $this->DATA = (object) ['status' => 404, 'status_msg' => 'Resource not found'];
        $this->GET = $this->input->get(null, true);
        $this->POST = $this->input->post(null, true);
        //$this->load->library('array2xml');
        //$this->load->library('oauth2lib');
        //$this->load->model('user_model', 'User');
        //$this->load->library('Notify', null, 'Notify');
        /*
        // non-destructive check if user is logged in and reading attributes
        if($this->oauth2lib->userHasScope('user')) {
            $this->account = $this->oauth2lib->getUser();
        }
        */
    }
    
    public function safeRequestVariable($key, $method='GET', $default=null) {
        switch($method) {
            case 'POST':
                return empty($this->POST[$key]) ? $default : $this->POST[$key];
            
            default:
                return empty($this->GET[$key]) ? $default : $this->GET[$key];
        }
    }

    public function set($key, $value = null) {
        $this->DATA->$key = $value;
    }

    public function set_all($json) {
        if (is_array($json) and ! is_object($json))
            $json = (object) $json;
        $this->DATA = $json;
    }

    function json($key) {
        if (!empty($this->DATA->$key))
            return $this->DATA->$key;
        else
            return false;
    }

    public function respond() {
        $format = empty($this->GET['format']) ? App::FORMAT_JSON : $this->GET['format'];
        switch ($format) {
            case App::FORMAT_JSON:
                $this->app->renderJson($this->DATA, ["HTTP/1.1 {$this->json('status')} {$this->json('status_msg')}"]);
                break;

            case App::FORMAT_XML:
                $data_array = json_decode(json_encode($this->DATA), TRUE);
                header("Content-Type: text/xml");
                $xmlData = Array2XML::createXML('Data', $data_array);
                echo $xmlData->saveXML();
                break;
        }
    }
    
    
    public function renderJson($data, $headers=null, $CORS=false) {
        if(!empty($headers) and is_array($headers)) {
            foreach($headers as $header) {
                $this->CI->output->set_header($header);
            }
        }
        if($CORS and defined('IN_GCLOUD') and IN_GCLOUD) {
            $this->output->set_header("Access-Control-Allow-Origin: *");
        }
        $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($data));
    }
    
}
