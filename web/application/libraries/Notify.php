<?php

class Notify {
    
    const CHANNEL_EMAIL = 'email';
    const CHANNEL_PUSH_IOS = 'ios.push_notification';
    const CHANNEL_PUSH_ANDROID = 'android.push_notification';
    private $channel;
    var $CI;
    
    public function __construct() {
        $this->CI =& get_instance();
        // set default channel
        $this->channel = self::CHANNEL_EMAIL;
    }
    
    public function setChannel( $channel=self::CHANNEL_EMAIL ) {
        $this->channel = $channel;
    }
    
    /** @TODO support for multi-channel message broadcasts **/
    public function sendMessage() {
        $config = func_get_arg(0);
        if(!is_array($config)) {
            throw new Exception("Message configuration is required", 400);
        }
        if(!empty($config['channel'])) {
            $this->channel = $config['channel'];
        }
        switch ($this->channel) {
            case self::CHANNEL_EMAIL:
                $required_config = ['to_email', 'subject', 'template', 'data'];
                foreach($required_config as $c) {
                    if(empty($config[$c])) {
                        throw new Exception("{$c} is a required configuration for Notify::sendMessage", 400);
                    }
                }
                if(!is_array($config['data']) && !is_object($config['data'])) {
                    throw new Exception("A data set must be passed containing parameters of the notification for Notify::sendMessage", 400);
                }
                // validation passed
                $html_msg = $this->CI->load->view($config['template'], $config['data'], true);
                // $this->CI->load->library('email');
                $this->CI->email->clear();
                $mailserverconfig = config_item('mailbox');
                $this->CI->email->initialize([
                    'mailtype'=>'html',
                    'protocol'=>'smtp',
                    'smtp_host'=>$mailserverconfig->host,
                    'smtp_port'=>$mailserverconfig->port,
                    'smtp_user'=>$mailserverconfig->username,
                    'smtp_pass'=>$mailserverconfig->password
                ]);
                $from_email = empty($config['from_email']) ? ADMIN_EMAIL : $config['from_email'];
                $from_name = empty($config['from_name']) ? APP_NAME : $config['from_name'];
                $this->CI->email->from($from_email, $from_name);
                $this->CI->email->to($config['to_email']);
                $this->CI->email->subject($config['subject']);
                $this->CI->email->message($html_msg);
                return $this->CI->email->send();
        }
    }
    
}
