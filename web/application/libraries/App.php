<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of App
 *
 * @author uchilaka
 */
class App {

    const FORMAT_JSON = 'json';
    const FORMAT_XML = 'xml';
    
    static function protocolToHTTP($url) {
        return preg_replace("/^https\:/", "http:", $url, 1);
    }
    
    static function protocolToHTTPS($url) {
        if(!DEV_MODE) {
            return preg_replace("/^http\:/", "https:", $url, 1);
        } else {
            return $url;
        }
    }
    
}
