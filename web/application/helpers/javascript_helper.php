<?php

function linkasset($uri, $type, $desc = null) {
    switch ($type) {
        case 'css':
            if (!empty($desc)) {
                echo <<<EOT
    <!-- {$desc} -->
EOT;
            }
            echo <<<EOT
    <link rel="stylesheet" href="{$uri}" />
EOT;
            break;

        case 'js':
            if (!empty($desc)) {
                echo <<<EOT
    <!-- {$desc} -->
EOT;
            }
            echo <<<EOT
    <script type="text/javascript" src="{$uri}"></script>
EOT;
            break;
    }
}
