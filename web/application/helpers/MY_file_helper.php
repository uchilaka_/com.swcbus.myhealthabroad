<?php
function makepath() {
    $bits = func_get_args();
    if(is_array($bits)) {
        return implode(DIRECTORY_SEPARATOR, $bits);
    }
}