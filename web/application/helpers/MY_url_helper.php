<?php
function webapi_url( $path, $secure = false ) {
    $url = API_BASE . $path;
    if( $secure ) {
        $url = preg_replace("/^http\:\/\//", "https://", $url);
    }
    return $url;
}

function browser_url ( $server_path, $secure = false ) {
    $url = str_replace(DIRECTORY_SEPARATOR, '/', str_replace( dirname(FCPATH) . DIRECTORY_SEPARATOR, WEBAPP_BASE, $server_path ));
    if($secure) {
        $url = preg_replace("/^http\:\/\//", "https://", $url);
    }
    return $url;
}

function resource_url( $uri, $resourceType=RESOURCETYPE_LOCAL ) {
    switch  ($resourceType) {
        case RESOURCETYPE_ENTERPRISE:
            return ENTERPRISE_BASE . $uri;
            
        default:
            return WEBAPP_BASE . $uri;
    }
}
